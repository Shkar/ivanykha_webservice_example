﻿using CustomersDbAccessLayer;
using AutoMapper;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using CustomersModel;
using CustomersModel.ViewModels;
using Newtonsoft.Json.Linq;

namespace CustomersWebApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]

    public class CustomerController : ControllerBase
    {
        private readonly ICustomersGetMethods _repositry;
        private readonly IMapper _mapper;

        public CustomerController(IMapper mapper, ICustomersGetMethods repositry)
        {
            _repositry = repositry;
            _mapper = mapper;
        }


        [HttpGet]
        public ActionResult <IEnumerable<Customer>> Get()
        {
            try
            {
                var result = _repositry.GetCustomerInfo();
                var mappedresult = _mapper.Map<IEnumerable<CustomerViewModel>>(result);
                if (mappedresult == null)
                {
                    return NotFound();
                }
                else
                {
                    return Ok(mappedresult);
                }
            }
            catch
            {
                return BadRequest();
            }
        }

        [HttpGet("{id}")]
        public ActionResult<Customer> Get(int id)
        {
            try
            {
                var result = _repositry.GetCustomerInfo(id);
                if(result == null)
                {
                    return NotFound();
                }
                else
                { 
                    return Ok(result);
                }
            }
            catch
            {
                return BadRequest();
            }
        }
    }
}

