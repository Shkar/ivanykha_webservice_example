﻿using System;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Linq;
using CustomersModel;

namespace CustomersDbAccessLayer
{
    public class CustomersGetMethods : ICustomersGetMethods
    {
        public CustomersGetMethods(CustomersContext customersContext)
        {
            _customersContext = customersContext;
        }

        private readonly CustomersContext _customersContext;

        public Customer GetCustomerInfo(int id)
        {
            return _customersContext.Customers
                .AsNoTracking()
                .Include(c => c.Transactions)
                .SingleOrDefault(c => c.Id == id);
        }

        public Customer GetCustomerInfo(string email)
        {
            return _customersContext.Customers
                .AsNoTracking()
                .Include(c => c.Transactions)
                .SingleOrDefault(c => c.Email == email);
        }

        public Customer GetCustomerInfo(int id, string email)
        {
            return _customersContext.Customers
                .AsNoTracking()
                .Include(c => c.Transactions)
                .SingleOrDefault(c => c.Id == id && c.Email == email);
        }

        public IEnumerable <Customer> GetCustomerInfo()
        {
            return _customersContext.Customers
                .AsNoTracking()
                .Include(c => c.Transactions)
                .ToList();
        }
    }
}
