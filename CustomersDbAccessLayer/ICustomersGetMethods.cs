﻿using CustomersModel;
using System.Collections.Generic;
namespace CustomersDbAccessLayer

{
    public interface ICustomersGetMethods
    {
        Customer GetCustomerInfo(int id);
        Customer GetCustomerInfo(string email);
        Customer GetCustomerInfo(int id, string email);
        IEnumerable<Customer> GetCustomerInfo();
    }
}