using System;
using Xunit;
using CustomersModel;
using System.Collections.Generic;
using CustomersDbAccessLayer;
using Moq;
using CustomersWebApi.Controllers;
using AutoMapper;
using CustomersModel.ViewModels;
using FluentAssertions;
using Microsoft.AspNetCore.Mvc;
using System.Web.Http.Results;

namespace Tests
{
    public class TestsCustomerController
    {
        private static IEnumerable<Customer> GetFakeListOfCustomers()
        {
            var fakeTransactionsList = new List<Transaction>
            {
                new Transaction {Id =1, CustomerId=1, AmountCurrency = "USD", AmountValue = 110, TransactionDate= new DateTime(2019-01-01), Status = "Failed" }
            };

            var fakeCustomerList = new List<Customer>
            {
                new Customer {Id=1, Name = "test_01", Email="test01@email.com", Mobile = 11 },
                new Customer {Id=2, Name = "test_02", Email="test01@email.com", Mobile = 12, Transactions =  fakeTransactionsList},
            };
            return fakeCustomerList;
        }


        private static Customer GetfakeCustomer(int id)
        {
            var fakeTransactionsList = new List<Transaction>
            {
                new Transaction {Id =1, CustomerId=1, AmountCurrency = "USD", AmountValue = 110, TransactionDate= new DateTime(2019-01-01), Status = "Failed" }
            };

            var fakeCustomer = new Customer
            {
                Id=1, Name = "fake customer 1", Email = "fake1@mail.com", Mobile = 123, Transactions=fakeTransactionsList
            };

            return fakeCustomer;
        }

        private CustomerController ArrangeMock()
        {
            var mockService = new Mock<ICustomersGetMethods>();
            mockService.Setup(x=>x.GetCustomerInfo()).Returns(()=> TestsCustomerController.GetFakeListOfCustomers());

            //mockService.Setup(x => x.GetCustomerInfo(It.IsAny<int>())).Returns(() => TestsCustomerController.GetfakeCustomer(1));

            var config = new MapperConfiguration(cfg => {
                cfg.CreateMap<Customer, CustomerViewModel>();
            });
            IMapper iMapper = config.CreateMapper();

            var controller = new CustomerController(iMapper, mockService.Object);
            return controller;

        }



        [Fact]
        public void GetCustomerInfo_returns_List()
        {
            //arrange
            var controller = ArrangeMock();

            //act
            var result = controller.Get();

            //assert
            
            result.Should().BeOfType(typeof(ActionResult<IEnumerable<Customer>>));
       
        }

        [Fact]
        public void GetCustomerInfoById_returns_Customer()
        {
            //arrange
            var controller = ArrangeMock();

            //act
            var result = controller.Get();

            //assert
            result.Should().BeOfType(typeof(ActionResult<Customer>));
            //result.Value.Id.Should().Be(1);

        }
    }
}
