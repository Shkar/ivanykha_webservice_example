﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CustomersModel.ViewModels
{
    public class CustomerViewModel
    {

        public int Id {get; set;}
        public string Name { get; set; }
        public string Email { get; set; }
        public int Mobile { get; set; }
   
        public List<TransactionViewModel> Transactions { get; set; }
    }
}
