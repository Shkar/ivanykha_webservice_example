﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CustomersModel.ViewModels
{
    public class TransactionViewModel
    {
        public DateTime TransactionDate { get; set; }
        public float AmountValue { get; set; }
        public string AmountCurrency { get; set; }
        public string Status { get; set; }
    }
}
