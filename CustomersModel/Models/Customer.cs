﻿using System.Collections.Generic;
using System;

namespace CustomersModel
{
    public class Customer
    {
        private int _id;
        private string _name;
        private string _email;
        private int _mobile;

        public int Id
        {
            get => _id;
            set
            {
                if (value.ToString().Length > 10)
                {
                    throw new ArgumentException("Customer Id should be less than 20 symbols");
                }
                _id = value;
            }
        }

        public string Name
        {
            get => _name;
            set
            {
                if (value.Length > 30)
                {
                    throw new ArgumentException("Customer Name should be less than 30 symbols");
                }
                _name = value;
            }
        }

        public string Email
        {
            get => _email;
            set
            {
                if (value.Length > 25)
                {
                    throw new ArgumentException("Customer Email should be less than 25 symbols");
                }
                _email = value;
            }
        }

        public int Mobile
        {
            get => _mobile;
            set
            {
                if (value.ToString().Length > 10)
                {
                    throw new ArgumentException("Customer Id should be less than 20 symbols");
                }
                _mobile = value;
            }
        }

        public List<Transaction> Transactions { get; set; }

    }
}