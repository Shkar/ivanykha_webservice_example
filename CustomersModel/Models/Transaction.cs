﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CustomersModel
{
    public class Transaction
    {
        public int Id { get; set; }
        public DateTime TransactionDate { get; set; }
        public float AmountValue { get; set; }
        public string AmountCurrency { get; set; }
        public string Status { get; set; }

        public int CustomerId { get; set; }
        public Customer Customer { get; set; }
    }
}
