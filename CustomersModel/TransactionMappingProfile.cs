﻿using CustomersModel.ViewModels;
using AutoMapper;

namespace CustomersModel
{
    public class TransactionMappingProfile : Profile
    {
        public TransactionMappingProfile()
        {
            CreateMap<Transaction, TransactionViewModel>();
        }
    }
}
