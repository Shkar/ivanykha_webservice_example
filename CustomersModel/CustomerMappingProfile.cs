﻿using AutoMapper;
using CustomersModel.ViewModels;


namespace CustomersModel
{
    public class CustomerMappingProfile : Profile
    {
        public CustomerMappingProfile ()
        {
            CreateMap<Customer, CustomerViewModel>();
        }
    }
}
